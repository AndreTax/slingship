﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Sound : MonoBehaviour
{
    public string soundName;
    public bool isPersistent;
    [HideInInspector] public float volume;

    private void Start()
    {
        if (isPersistent)
        {
            DontDestroyOnLoad(gameObject);
        }

        if (!GetComponent<AudioSource>().loop)
        {
            float lenght = GetComponent<AudioSource>().clip.length;
            Destroy(gameObject, lenght);
        }
    }
}
