﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    AudioSource musicSource;
    public List<Sound> loopingSounds;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            loopingSounds = new List<Sound>();
            musicSource = GetComponent<AudioSource>();
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            instance.loopingSounds.Clear();
            Destroy(gameObject);
            return;
        }
    }

    public void Play(Sound sound, Vector2 atPosition)
    {
        Sound s = Instantiate(sound, atPosition, Quaternion.identity);
        if(s.GetComponent<AudioSource>().loop)
        {
            loopingSounds.Add(s);
        }
    }

    public void PlayMusic(Sound sound)
    {
        Sound s = Instantiate(sound, Vector3.zero, Quaternion.identity);
    }

    public void StopLoopingSound(Sound sound)
    {
        Sound s = loopingSounds.Find(x => x.soundName.Equals(sound.soundName));
        if(s != null)
        {
            loopingSounds.Remove(s);
            Destroy(s.gameObject);
        }
    }
}
