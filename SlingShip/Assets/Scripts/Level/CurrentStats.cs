﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CurrentStats", menuName = "ScriptableObjects/CurrentStats", order = 1)]
public class CurrentStats : ScriptableObject
{
    public List<int> currentTier; //how many arrow will be displayied in UI
}
