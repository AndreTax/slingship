﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum endGameResult { Win, Lost };

public class GameManager : MonoBehaviour
{
    [HideInInspector] public bool isGameStarted;
    [HideInInspector] public bool isGameOver;
    public int levelIndex;
    public CurrentStats currentStats;
    public static GameManager instance;
    public Vector3 currentDirection;
    public float matchTime;
    public float currentSpeed;
    public float startMatchDelay;
    public int usableSlings;
    public bool isTutorial;
    public string tutorialMessage;
    public int barrelsInScene;
    bool isGamePaused;
    bool winCondition = false;
    public Sound seaSound;
    public Sound levelTheme;
    public Sound levelSuccess;
    public Sound levelFail;
    public Action onGamePreparing;
    public Action onGameStart;
    public Action<endGameResult> onGameOver;
    public Action<bool> onGamePaused;

    private void Awake()
    {
       instance = this;
    }

    private void Start()
    {
        AudioManager.instance.Play(seaSound, transform.position);
        AudioManager.instance.PlayMusic(levelTheme);
        StartCoroutine(StartCountDown());
    }

    IEnumerator StartCountDown()
    {
        onGamePreparing?.Invoke();
        yield return new WaitForSeconds(startMatchDelay);
        onGameStart?.Invoke();
        isGameStarted = true;
    }

    private void Update()
    {
        if (!isGameStarted || isGameOver)
            return;

        matchTime -= Time.deltaTime;
        if(matchTime <= 0 && !winCondition)
        {
            isGameStarted = false;
            GameOver();
        }
    }

    public void ChestTaken()
    {
        winCondition = true;
    }

    public void BarrelTaken()
    {
        barrelsInScene--;
    }

    public void CheckGameOver()
    {
        if (winCondition)
        {
            GameWon();
        }
        else if (usableSlings <= 0 && !isGameOver)
        {
            GameOver();
        }
    }

    public void GameOver()
    {
        AudioManager.instance.Play(levelFail, transform.position);
        isGameOver = true;
        onGameOver(endGameResult.Lost);
    }

    public void GameWon()
    {
        AudioManager.instance.Play(levelSuccess, transform.position);
        onGameOver(endGameResult.Win);
        isGameOver = true;
        if (PlayerState.instance != null)
        {
            PlayerState.instance.levelStates[levelIndex].isCompleted = true;
            if (barrelsInScene == 0)
            {
                PlayerState.instance.levelStates[levelIndex].isPerfectScore = true;
            }
        }
    }

    public void GamePause(bool pause)
    {
        isGamePaused = pause;
        Time.timeScale = isGamePaused ? 0 : 1;
        onGamePaused?.Invoke(isGamePaused);
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    #region ActionsRegistrations

    public void RegisterOnGamePreparing(Action action)
    {
        onGamePreparing += action;
    }

    public void UnRegisterOnGamePreparing(Action action)
    {
        onGamePreparing -= action;
    }

    public void RegisterOnGameStart(Action action)
    {
        onGameStart += action;
    }

    public void UnRegisterOnGameStart(Action action)
    {
        onGameStart -= action;
    }

    public void RegisterOnGameOver(Action<endGameResult> action)
    {
        onGameOver += action;
    }

    public void UnRegisterOnGameOver(Action<endGameResult> action)
    {
        onGameOver -= action;
    }

    public void RegisterOnGamePaused(Action<bool> action)
    {
        onGamePaused += action;
    }

    public void UnRegisterOnGamePaused(Action<bool> action)
    {
        onGamePaused -= action;
    }


    #endregion
}
