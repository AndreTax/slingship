﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : MonoBehaviour
{
    public bool canMove;
    public bool forceDirection;
    public Vector3 forcedDirection;
    public float speed;
    public float waterResistance;

    protected abstract void OnTriggerEnter(Collider other);

    void Update()
    {
        if (canMove && GameManager.instance.isGameStarted)
        {
            if (forceDirection)
            {
                transform.Translate(forcedDirection * speed * Time.deltaTime, Space.World);
            }
            else
            {
                transform.Translate(GameManager.instance.currentDirection * (GameManager.instance.currentSpeed - waterResistance) * Time.deltaTime, Space.World);
            }
        }
    }
}
