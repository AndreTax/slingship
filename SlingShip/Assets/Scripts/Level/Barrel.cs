﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrel : Interactable
{
    public Sound barrelTaken;

    protected override void OnTriggerEnter(Collider other)
    {
        if (!GameManager.instance.isGameOver && other.GetComponent<Ship>() != null)
        {
            GameManager.instance.BarrelTaken();
            AudioManager.instance.Play(barrelTaken,transform.position);
            Destroy(gameObject);
        }
    }
}
