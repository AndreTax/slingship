﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Treasure : MonoBehaviour
{
    public Sound TreasureTaken;

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Ship>() != null)
        {
            GameManager.instance.ChestTaken();
            AudioManager.instance.Play(TreasureTaken,transform.position);
            Destroy(gameObject);
        }
    }
}
