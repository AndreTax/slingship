﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : Interactable
{
    protected override void OnTriggerEnter(Collider other)
    {
        if (!GameManager.instance.isGameOver)
        {
            Ship ship = other.GetComponent<Ship>();
            if (other.GetComponent<Ship>() != null)
            {
                GameManager.instance.GameOver();
                ship.Explode();
            }
        }
    }
}
