﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    public int levelIndex;
    public Sprite completedSprite;
    public Sprite perfectScoreSprite;
    public Image buttonImage;
    public Image scoreImage;
    public Text levelIndexText;
    public Sound clickSound;
    public Sound mouseOverSound;
    LevelState levelState;
    
    void Start()
    {
        //check if the level is completed and what's the score
        levelState = PlayerState.instance.levelStates[levelIndex];
        levelIndexText.text = (levelState.levelIndex + 1).ToString();
        if (levelState.isCompleted)
        {
            buttonImage.sprite = completedSprite;
            if(levelState.isPerfectScore)
            {
                scoreImage.sprite = perfectScoreSprite;
            }
            scoreImage.enabled = true;
        }      
    }

    public void OnClick()
    {
        AudioManager.instance.Play(clickSound, transform.position);
        SceneManager.LoadScene(levelState.levelName);
    }

    public void OnMouseOver()
    {
        AudioManager.instance.Play(mouseOverSound, transform.position);        
    }
}
