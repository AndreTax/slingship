﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlingLeft : MonoBehaviour
{
    public Text slingText;
    InputManager inputManager;
    string baseText;

    // Start is called before the first frame update
    void Start()
    {
        inputManager = FindObjectOfType<InputManager>();
        baseText = slingText.text;
        slingText.text += " " + GameManager.instance.usableSlings;
        inputManager.RegisterOnDragEnd(DragEnd);
    }

    void DragEnd(bool result)
    {
        if(result)
        {
            slingText.text = baseText + " " + GameManager.instance.usableSlings;
        }
    }
}
