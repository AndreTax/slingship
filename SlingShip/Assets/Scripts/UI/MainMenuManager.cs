﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public Sound mainMenuTheme;

    private void Start()
    {
        AudioManager.instance.PlayMusic(mainMenuTheme);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
