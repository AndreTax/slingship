﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DragPointer : MonoBehaviour
{
    InputManager inputManager;
    public Image dragStartImage;
    public Image trail;
    bool dragging;
    Vector3 startDragPos;
    public Gradient colorGradient;

    void Start()
    {
        //temp
        inputManager = FindObjectOfType<InputManager>();
        inputManager.RegisterOnStartDrag(DragStart);
        inputManager.RegisterOnDragEnd(DragEnd);
        inputManager.RegisterOnDragging(ChangeColor);
    }

    private void OnDisable()
    {
        inputManager.UnRegisterOnStartDrag(DragStart);
        inputManager.UnRegisterOnDragEnd(DragEnd);
        inputManager.UnRegisterOnDragging(ChangeColor);
    }

    private void Update()
    {
        if(dragging)
        {
            Vector3 diff = (startDragPos - Input.mousePosition);
            trail.gameObject.transform.localScale = new Vector3(transform.localScale.x, diff.magnitude / 100, transform.localScale.z);
            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            trail.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
            dragStartImage.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        }
    }

    void DragStart(Vector3 startPos)
    {
        startDragPos = startPos;
        trail.gameObject.transform.position = startPos;
        dragStartImage.gameObject.transform.position = startPos;
        trail.enabled = true;
        dragStartImage.enabled = true;
        dragging = true;
    }

    void DragEnd(bool result)
    {
        dragStartImage.enabled = false;
        trail.enabled = false;
        dragging = false;
    }

    void ChangeColor(float current, float max)
    {
        dragStartImage.color = colorGradient.Evaluate(current / max);
        trail.color = colorGradient.Evaluate(current / max);
    }


}
