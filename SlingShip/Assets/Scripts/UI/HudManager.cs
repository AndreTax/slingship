﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HudManager : MonoBehaviour
{
    public string winMessage;
    public Color winColor;
    public string loseMessage;
    public Color loseColor;
    public GameObject gameOverPanel;
    public Text gameOverText;
    public Text timerText;
    public GameObject pausePanel;
    bool startTimer;
    float startTime;
    public float timerChangeColorPerc;
    bool colorChanged;

    // Start is called before the first frame update
    void Start()
    {
        UpdateTimer();
        GameManager.instance.RegisterOnGameStart(GameStart);
        GameManager.instance.RegisterOnGameOver(GameOver);
        GameManager.instance.RegisterOnGamePaused(GamePaused);
    }

    private void OnDisable()
    {
        GameManager.instance.UnRegisterOnGameStart(GameStart);
        GameManager.instance.UnRegisterOnGameOver(GameOver);
    }

    void GameStart()
    {
        startTime = GameManager.instance.matchTime;
        startTimer = true;
    }

    void GameOver(endGameResult result)
    {
        if (result == endGameResult.Win)
        {
            gameOverText.text = winMessage;
            gameOverText.color = winColor;
        }
        else
        {
            gameOverText.text = loseMessage;
            gameOverText.color = loseColor;
        }
        gameOverPanel.SetActive(true);
        gameOverPanel.GetComponent<Animator>().SetTrigger("Show");
    }

    void GamePaused(bool isPaused)
    {
        if(isPaused)
        {
            OpenPausePanel();
        }
        else
        {
            ClosePausePanel();
        }
    }

    void Update()
    {
        if (!startTimer)
            return;

        UpdateTimer();
    }

    void UpdateTimer()
    {
        float time = GameManager.instance.matchTime;
        int minutes = (int)time / 60;
        int seconds = (int)time % 60;
        timerText.text = minutes.ToString().PadLeft(2, '0') + ":" + seconds.ToString().PadLeft(2, '0');
        if(!colorChanged && time / startTime <= timerChangeColorPerc / 100)
        {
            colorChanged = true;
            timerText.color = Color.red;
        }
        if (minutes + seconds <= 0)
        {
            startTimer = false;
        }
    }

    void OpenPausePanel()
    {
        Time.timeScale = 0;
        pausePanel.SetActive(true);
    }

    void ClosePausePanel()
    {
        Time.timeScale = 1;
        pausePanel.SetActive(false);
    }

    public void OnResumeClick()
    {
        ClosePausePanel();
        GameManager.instance.GamePause(false);
    }

    public void OnRestartClick()
    {
        Time.timeScale = 1;
        GameManager.instance.RestartLevel();
    }

    public void OnMainMenuClick()
    {
        Time.timeScale = 1;
        GameManager.instance.GoToMainMenu();
    }

    public void OnNextLevelClick()
    {
        if (GameManager.instance.levelIndex < PlayerState.instance.levelsCount - 1)
        {
            SceneManager.LoadScene("Level" + (GameManager.instance.levelIndex + 1));
        }
        else
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}
