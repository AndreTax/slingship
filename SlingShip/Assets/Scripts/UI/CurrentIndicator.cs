﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentIndicator : MonoBehaviour
{
    public GameObject currentsParent;

    private void Start()
    {
        for (int i = 0; i < 3; i++)
        {
            if (GameManager.instance.currentSpeed >= GameManager.instance.currentStats.currentTier[i])
            {
                Transform arrow = currentsParent.transform.GetChild(i);
                var angle = Mathf.Atan2(GameManager.instance.currentDirection.z, GameManager.instance.currentDirection.x) * Mathf.Rad2Deg;
                arrow.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                arrow.gameObject.SetActive(true);
            }
        }
    }
}
