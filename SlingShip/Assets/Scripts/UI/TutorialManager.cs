﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour
{
    public GameObject tutorialBox;
    public Text tutorialText;

    void Start()
    {
        if(GameManager.instance.isTutorial && !PlayerState.instance.levelStates[GameManager.instance.levelIndex].isCompleted)
        {
            GameManager.instance.GamePause(true);
            ShowTutorialBox();
        }
    }
    
    void ShowTutorialBox()
    {
        tutorialText.text = GameManager.instance.tutorialMessage;
        tutorialBox.SetActive(true);
    }

    public void OnConfirmClick()
    {
        tutorialBox.SetActive(false);
        GameManager.instance.GamePause(false);
    }
}
