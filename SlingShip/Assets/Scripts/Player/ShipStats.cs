﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ShipStats", menuName = "ScriptableObjects/ShipStats", order = 1)]
public class ShipStats : ScriptableObject
{
    public float minSpeed;
    public float maxSpeed;
    public AnimationCurve deceleration;
}
