﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelState
{
    public LevelState(int _levelIndex)
    {
        levelIndex = _levelIndex;
        levelName = "Level" + _levelIndex;
    }
    public int levelIndex;
    public string levelName;
    [HideInInspector] public bool isCompleted;
    [HideInInspector] public bool isPerfectScore;
}
