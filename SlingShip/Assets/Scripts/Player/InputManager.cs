﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputManager : MonoBehaviour
{
    public LineRenderer trajectory;
    public float dragDistanceMultiplier;
    public int trajectoryNodes;
    public Image draggingIcon;
    public Sound dragSound;
    Ship ship;
    Vector3 dragStartPos;
    Vector3 dragEndPos;
    bool dragStart;
    bool canUseInput = false;
    Action onDragRelease;
    Action<Vector3> onStartDrag;
    Action<float,float> onDragging;
    Action<bool> onDragEnd;

    private void Start()
    {
        dragStart = false;
        ship = GetComponent<Ship>();
        GameManager.instance.RegisterOnGameStart(GameStarted);
        GameManager.instance.RegisterOnGameOver(GameOver);
        GameManager.instance.RegisterOnGamePaused(GamePause);
    }

    private void OnDisable()
    {
        GameManager.instance.UnRegisterOnGameStart(GameStarted);
        GameManager.instance.UnRegisterOnGameOver(GameOver);
        GameManager.instance.UnRegisterOnGamePaused(GamePause);
    }

    void GameStarted()
    {
        canUseInput = true;
    }

    void GameOver(endGameResult result)
    {
        StopDrag(false);
        canUseInput = false;
    }

    void GamePause(bool isPaused)
    {
        if (!GameManager.instance.isGameOver)
        {
            canUseInput = !isPaused;
        }
    }

    void Update()
    {
        if(GameManager.instance.isGameOver)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (dragStart && !ship.moving) //cancel the drag if i pause the game
            {
                StopDrag(false);
            }
            GameManager.instance.GamePause(true);
        }

        if (!canUseInput)
        {
            return;
        }

        if(!dragStart && !ship.moving && Input.GetMouseButtonDown(0))         //Drag Start
        {
            dragStart = true;
            AudioManager.instance.Play(dragSound, transform.position);
            draggingIcon.enabled = true;
            dragStartPos = Input.mousePosition;
            onStartDrag?.Invoke(dragStartPos);
        }
        else if(dragStart && !ship.moving && Input.GetMouseButtonUp(0))        //Drag End
        {
            if (ship.points.Count > 0)
            {
                GameManager.instance.usableSlings--;
                AudioManager.instance.Play(ship.shipMove, transform.position);
                ship.moving = true;
                ship.currentSpeed = ship.startSpeed;
                StopDrag(true);
            }
            else
            {
                StopDrag(false);
            }
        }

        if(dragStart && !ship.moving)        //Dragging
        {
            Dragging();
        }
    }

    void StopDrag(bool slingDone)
    {
        draggingIcon.enabled = false;
        dragStart = false;
        onDragging?.Invoke(0, ship.stats.maxSpeed);
        onDragEnd?.Invoke(slingDone);
        AudioManager.instance.StopLoopingSound(dragSound);
        if (!slingDone)
            trajectory.positionCount = 0;
    }

    void Dragging()
    {
        if(Input.mousePosition == dragEndPos) //no need to recalculate
        {
            return;
        }
        dragEndPos = Input.mousePosition;

        //check if the drag angle is ok
        float angle = Mathf.Atan2(dragEndPos.y - dragStartPos.y, dragEndPos.x - dragStartPos.x) * Mathf.Rad2Deg;
        if (angle > 0)
        {
            StopDrag(false);
            return;
        }

        //calculate speed from the drag distance and update the ui arrow
        float tempDistance = (dragEndPos - dragStartPos).magnitude * dragDistanceMultiplier;
        float distance = Mathf.Clamp(tempDistance, ship.stats.minSpeed, ship.stats.maxSpeed);
        ship.currentSpeed = distance;
        ship.startSpeed = ship.currentSpeed;
        onDragging?.Invoke(ship.currentSpeed, ship.stats.maxSpeed);

        //convert screen coords into world coords
        Vector2 theStartmousePos = new Vector2();
        theStartmousePos.x = dragStartPos.x;
        theStartmousePos.y = Camera.main.pixelHeight - dragStartPos.y;
        Vector3 startpoint = Camera.main.ScreenToWorldPoint(new Vector3(-theStartmousePos.x, theStartmousePos.y, Camera.main.nearClipPlane));
        Vector2 theEndmousePos = new Vector2();
        theEndmousePos.x = dragEndPos.x;
        theEndmousePos.y = Camera.main.pixelHeight - dragEndPos.y;
        Vector3 endpoint = Camera.main.ScreenToWorldPoint(new Vector3(-theEndmousePos.x, theEndmousePos.y, Camera.main.nearClipPlane));
        Vector3 direction = (endpoint - startpoint).normalized;

        //init waypoints
        ship.points = new List<Vector3>();
        ship.points.Add(new Vector3(transform.position.x, 0, transform.position.z));
        
        //Updating values in deceleration curve
        Keyframe[] keys = ship.stats.deceleration.keys;
        keys[0].value = ship.currentSpeed;
        keys[1].value = ship.stats.minSpeed;
        keys[1].time = trajectoryNodes;
        ship.stats.deceleration.keys = keys;

        //fill the list of trajectory point
        Vector3 lastPos = transform.position;
        for (int i = 0; i < trajectoryNodes; i++)
        {
            ship.currentSpeed = ship.stats.deceleration.Evaluate(i) / trajectoryNodes;
            Vector3 point = lastPos + (direction * ship.currentSpeed) + (GameManager.instance.currentDirection * (GameManager.instance.currentSpeed / trajectoryNodes));//* (GameManager.instance.currentSpeed - currentSpeed)
            ship.points.Add(new Vector3(point.x, 0, point.z));
            lastPos = point;
        }

        //if is a tutorial level show the line renderer
        if (GameManager.instance.isTutorial)
        {
            trajectory.positionCount = ship.points.Count;
            for (int i = 0; i < ship.points.Count; ++i)
            {
                trajectory.SetPosition(i, new Vector3(ship.points[i].x, 1, ship.points[i].z));
            }
        }
    }

    #region ActionRegistrations
    public void RegisterOnStartDrag(Action<Vector3> action)
    {
        onStartDrag += action;
    }
    public void UnRegisterOnStartDrag(Action<Vector3> action)
    {
        onStartDrag -= action;
    }

    public void RegisterOnDragging(Action<float,float> action)
    {
        onDragging += action;
    }
    public void UnRegisterOnDragging(Action<float, float> action)
    {
        onDragging -= action;
    }

    public void RegisterOnDragEnd(Action<bool> action)
    {
        onDragEnd += action;
    }
    public void UnRegisterOnDragEnd(Action<bool> action)
    {
        onDragEnd -= action;
    }

    #endregion
}
