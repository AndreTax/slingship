﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState : MonoBehaviour
{
    public static PlayerState instance;
    public int levelsCount;
    [HideInInspector] public List<LevelState> levelStates = new List<LevelState>();

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            for (int i = 0; i < levelsCount; i++)
            {
                levelStates.Add(new LevelState(i));
            }
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }
}
