﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    [HideInInspector] public bool moving = false;
    [HideInInspector] public List<Vector3> points = new List<Vector3>();
    [HideInInspector] public float currentSpeed;
    [HideInInspector] public float startSpeed;
    [HideInInspector] public ShipStats stats;
    public ShipStats defaultStats;
    Renderer rend;
    public Sound shipMove;
    public Sound shipDestruction;
    public GameObject explosionVfx;
    int currentPointsCount;

    // Start is called before the first frame update
    void Awake()
    {
        stats = ScriptableObject.Instantiate(defaultStats);
        rend = GetComponentInChildren<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        //if i'm outside the level
        if(!rend.isVisible && GameManager.instance.isGameStarted && !GameManager.instance.isGameOver)
        {
            GameManager.instance.GameOver();
            Destroy(gameObject);
        }

        //use the waypoints to move
        if (moving)
        {
            transform.position = Vector3.MoveTowards(transform.position, points[0], currentSpeed * Time.deltaTime);
            if (transform.position == points[0])
            {
                points.RemoveAt(0);
                if (points.Count > 0)
                {
                    transform.LookAt(points[0]);
                    currentSpeed = stats.deceleration.Evaluate(currentPointsCount - points.Count);
                }
                else  //end of movement, i need to check if is a gameover or o victory
                {
                    moving = false;
                    GameManager.instance.CheckGameOver();
                }
            }
        }
        else if (points.Count > 1) //i'm still dragging, rotate to the first node
        {
            currentPointsCount = points.Count;
            transform.LookAt(points[1]);
        }
    }

    public void Explode() //goodbye ship
    {
        AudioManager.instance.Play(shipDestruction, transform.position);
        Instantiate(explosionVfx, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
